import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.concurrent.TimeUnit;

public class UPDClient implements Runnable {

    private DatagramSocket socket;
    private InetAddress address;
    private int port;
    private int maxSizePackage = 8;
    private byte[] buf = new byte[maxSizePackage];

    public UPDClient(String ip, int port) throws Exception {
        this.address = InetAddress.getByName(ip);
        this.port = port;
        this.socket = new DatagramSocket();
    }

    @Override
    public void run() {

        System.out.println("Client start");

        for (int i = 1; ; i++) {


            if (i % 10 != 0) {
                try {
                    byte[] messagePing = "ping".getBytes();
                    buf = new byte[maxSizePackage];
                    DatagramPacket packetPingToServer = new DatagramPacket(messagePing, messagePing.length,
                            address, port);
                    DatagramPacket packetPongFromServer = new DatagramPacket(buf, buf.length);
                    socket.send(packetPingToServer);
                    socket.receive(packetPongFromServer);

                    System.out.println("Server: " + new String(packetPongFromServer.getData()));

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {

                try {

                    byte[] messageGetNotify = "get".getBytes();
                    buf = new byte[maxSizePackage];
                    DatagramPacket packetNotifyGetToServer = new DatagramPacket(messageGetNotify, messageGetNotify.length,
                            address, port);
                    DatagramPacket packetNotifyFromServer = new DatagramPacket(buf, buf.length);
                    socket.receive(packetNotifyFromServer);
                    System.out.println("Server: " + new String(packetNotifyFromServer.getData()));
                    socket.send(packetNotifyGetToServer);

                    TimeUnit.SECONDS.sleep(2);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }

    }

}
