public class Main {

    public static void main(String[] args) throws Exception {

        int port = 8080;

        UPDClient client = new UPDClient("localhost", port);
        UPDServer server = new UPDServer(port);

        new Thread(server).start();
        Thread.sleep(1000);
        new Thread(client).start();

    }

}
