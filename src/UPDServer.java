import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.concurrent.TimeUnit;

public class UPDServer implements Runnable {

    private int maxSizePackage = 8;
    private byte[] buf = new byte[maxSizePackage];
    private InetAddress address;
    private int port;
    private DatagramSocket socket;

    public UPDServer(int port) throws Exception {
        this.socket = new DatagramSocket(port);
    }

    @Override
    public void run() {

        System.out.println("Server start");

        for (int i = 1; ; i++) {

            if (i % 10 != 0) {
                try {
                    byte[] responsePong = "pong".getBytes();
                    buf = new byte[maxSizePackage];
                    DatagramPacket packetPingFromClient = new DatagramPacket(buf, buf.length);
                    socket.receive(packetPingFromClient);
                    address = packetPingFromClient.getAddress();
                    port = packetPingFromClient.getPort();

                    System.out.println("Client: " + new String(packetPingFromClient.getData()));

                    DatagramPacket packetPongToClient = new DatagramPacket(responsePong, responsePong.length,
                            address, port);
                    socket.send(packetPongToClient);

                    TimeUnit.SECONDS.sleep(2);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    byte[] responseGet = "notify".getBytes();
                    buf = new byte[maxSizePackage];
                    DatagramPacket packetNotifyGetFromClient = new DatagramPacket(buf, buf.length);
                    DatagramPacket packetNotifyToClient = new DatagramPacket(responseGet, responseGet.length,
                            address, port);
                    socket.send(packetNotifyToClient);
                    socket.receive(packetNotifyGetFromClient);

                    System.out.println("Client: " + new String(packetNotifyGetFromClient.getData()));

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }

    }

}
